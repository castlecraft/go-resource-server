package main

import (
	"context"
	"encoding/json"
	"log"
	"net/http"
	"time"

	authControllers "go-resource-server/auth/controllers"
	authEntities "go-resource-server/auth/entities"
	middlewares "go-resource-server/auth/middlewares"
	authPolicies "go-resource-server/auth/policies"
	schedulers "go-resource-server/auth/schedulers"
	common "go-resource-server/common"

	"github.com/gorilla/mux"
	"github.com/jasonlvhit/gocron"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// App base struct to connect router and database
type App struct {
	Database *mongo.Database
	Router   *mux.Router
}

// Initialize initializes the database using environment variables
func (a *App) Initialize(dbhost string, dbname string) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, _ := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://"+dbhost+":27017"))
	a.Database = client.Database(dbname)
	a.Router = mux.NewRouter()
	a.initializeRoutes()
	a.runCron()
}

func (a *App) initializeRoutes() {
	setupController := authControllers.SetupController{}
	a.Router.HandleFunc("/info", a.info).Methods(http.MethodGet)
	a.Router.HandleFunc(
		"/guarded",
		common.Chain(
			a.guarded,
			middlewares.RoleGuard([]string{authPolicies.RoleAdministrator}),
			middlewares.TokenGuard(a.Database),
		),
	).Methods(http.MethodGet)
	a.Router.HandleFunc(
		"/setup",
		setupController.SetupEndpoint(a.Database),
	).Methods(http.MethodPost)
}

// Run starts the app with routes
func (a *App) Run(addr string) {
	log.Println("Listening on ", addr)
	http.ListenAndServe(addr, a.Router)
}

func (a *App) info(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var settings authEntities.AuthSettings
	collection := a.Database.Collection(authEntities.AuthSettingsCollection)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	mSettings := collection.FindOne(ctx, bson.M{})
	mSettings.Decode(&settings)
	info := make(map[string]interface{})
	info["service"] = "go-resource-server"
	if settings.AppURL != "" {
		info["appURL"] = settings.AppURL
	}
	if settings.AuthServerURL != "" {
		info["authServerURL"] = settings.AuthServerURL
	}
	if settings.ClientID != "" {
		info["clientId"] = settings.ClientID
	}
	if settings.ProfileURL != "" {
		info["profileURL"] = settings.ProfileURL
	}
	if settings.TokenURL != "" {
		info["tokenURL"] = settings.TokenURL
	}
	if settings.IntrospectionURL != "" {
		info["introspectionURL"] = settings.IntrospectionURL
	}
	if settings.AuthorizationURL != "" {
		info["authorizationURL"] = settings.AuthorizationURL
	}
	if settings.CallbackURLs != nil {
		info["callbackURLs"] = settings.CallbackURLs
	}
	if settings.RevocationURL != "" {
		info["revocationURL"] = settings.RevocationURL
	}
	json.NewEncoder(w).Encode(info)
}

func (a *App) guarded(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Write([]byte(`{"message":"success"}`))
}

func (a *App) runCron() {
	go func() {
		gocron.Every(900).Seconds().Do(
			schedulers.DeleteExpiredTokensScheduler,
			a.Database,
		)
		<-gocron.Start()
	}()
}
