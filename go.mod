module go-resource-server

go 1.12

require (
	github.com/c9s/gomon v0.0.0-20180201045451-52a3eced35f0 // indirect
	github.com/caarlos0/env v3.5.0+incompatible
	github.com/daviddengcn/go-colortext v0.0.0-20180409174941-186a3d44e920 // indirect
	github.com/deckarep/gosx-notifier v0.0.0-20180201035817-e127226297fb // indirect
	github.com/everdev/mack v0.0.0-20180604194106-83ad607f6010 // indirect
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/golang/snappy v0.0.1 // indirect
	github.com/google/go-cmp v0.2.0 // indirect
	github.com/gorilla/context v1.1.1
	github.com/gorilla/mux v1.7.1
	github.com/howeyc/fsnotify v0.9.0 // indirect
	github.com/jasonlvhit/gocron v0.0.0-20190402024347-5bcdd9fcfa9b
	github.com/joho/godotenv v1.3.0
	github.com/juliangruber/go-intersect v1.0.0
	github.com/mattn/go-colorable v0.1.1 // indirect
	github.com/mattn/go-gntp v0.0.0-20171028142259-d20bc996b11f // indirect
	github.com/mgutz/ansi v0.0.0-20170206155736-9520e82c474b // indirect
	github.com/sirupsen/logrus v1.4.1 // indirect
	github.com/stretchr/testify v1.3.0 // indirect
	github.com/thedevsaddam/govalidator v1.9.6
	github.com/tidwall/pretty v0.0.0-20190325153808-1166b9ac2b65 // indirect
	github.com/x-cray/logrus-prefixed-formatter v0.5.2 // indirect
	github.com/xdg/scram v0.0.0-20180814205039-7eeb5667e42c // indirect
	github.com/xdg/stringprep v1.0.0 // indirect
	go.mongodb.org/mongo-driver v1.0.0
	golang.org/x/crypto v0.0.0-20190411191339-88737f569e3a // indirect
	golang.org/x/sync v0.0.0-20190412183630-56d357773e84 // indirect
	golang.org/x/text v0.3.0 // indirect
)
