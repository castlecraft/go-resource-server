package main

import (
	common "go-resource-server/common"
)

func main() {
	cfg := common.ConfigureEnvironment()
	a := App{}
	a.Initialize(cfg.DatabaseHost, cfg.DatabaseName)
	a.Run(":8080")
}
