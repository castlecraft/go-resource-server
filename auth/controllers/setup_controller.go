package controllers

import (
	"encoding/json"
	"net/http"

	authCommands "go-resource-server/auth/commands"
	commonHandlers "go-resource-server/common"

	"go.mongodb.org/mongo-driver/mongo"
)

// SetupController One time setup for server
type SetupController struct {
}

// SetupEndpoint handler function for update settings
// takes mongo Database as argument
func (c *SetupController) SetupEndpoint(db *mongo.Database) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("content-type", "application/json")
		statusCode, err := authCommands.SetupServerCommand(w, r, db)
		if err != nil {
			var message interface{}
			json.Unmarshal([]byte(err.Error()), &message)
			commonHandlers.RespondWithError(w, statusCode, message)
		}
	}
}
