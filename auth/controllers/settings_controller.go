package controllers

import (
	"net/http"

	"go.mongodb.org/mongo-driver/mongo"
)

// SettingsController Controller for Settings
type SettingsController struct {
}

// UpdateSettingsEndpoint handler function for update settings
// takes mongo Database as argument
func (c *SettingsController) UpdateSettingsEndpoint(db *mongo.Database) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

	}
}
