package middlewares

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"net/url"
	"strings"
	"time"

	authEntities "go-resource-server/auth/entities"
	common "go-resource-server/common"

	gorillaContext "github.com/gorilla/context"
	"github.com/juliangruber/go-intersect"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

// TokenGuard used to guard endpoints with access_token
func TokenGuard(database *mongo.Database) common.Middleware {
	return func(f http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			isValid, err := isRequestValid(r, database)
			if err != nil {
				isValid = false
			}
			if isValid {
				f(w, r)
			} else {
				var message interface{}
				json.Unmarshal([]byte(`{"Error":"Unauthorized"}`), &message)
				common.RespondWithError(w, http.StatusUnauthorized, message)
			}
		}
	}
}

func isRequestValid(r *http.Request, database *mongo.Database) (bool, error) {
	const tokenKey = "token"
	tokenCollection := database.Collection(authEntities.TokenCacheCollection)
	settingsCollection := database.Collection(authEntities.AuthSettingsCollection)
	tokenHeader := strings.Split(r.Header.Get("Authorization"), " ")
	tokenQuery := r.URL.Query().Get("access_token")
	isValidHeader := len(tokenHeader) > 1 && strings.ToLower(tokenHeader[0]) == "bearer"
	isValid := false
	if isValidHeader || tokenQuery != "" {
		// Check local token
		var token authEntities.TokenCache
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()
		var accessToken string
		if isValidHeader {
			accessToken = tokenHeader[1]
		} else if tokenQuery != "" {
			accessToken = tokenQuery
		}
		mToken := tokenCollection.FindOne(ctx, bson.M{
			"accessToken": accessToken,
		})
		mToken.Decode(&token)
		if err := mToken.Err(); err != nil {
			isValid = false
			return isValid, err
		}
		if token.ExpiryTime > time.Now().Unix() {
			isValid = true
			gorillaContext.Set(r, tokenKey, token)
			return isValid, nil
		}
		if token.AccessToken == "" {
			// introspect token
			tokenCache, err := introspectToken(settingsCollection, tokenCollection, accessToken)
			if err != nil {
				isValid = false
				return isValid, nil
			}
			isValid = true
			gorillaContext.Set(r, tokenKey, tokenCache)
			return isValid, nil
		}
	}
	return isValid, nil
}

func introspectToken(
	settingsCollection *mongo.Collection,
	tokenCollection *mongo.Collection,
	accessToken string,
) (authEntities.TokenCache, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	var settings authEntities.AuthSettings
	mSettings := settingsCollection.FindOne(ctx, bson.M{})
	mSettings.Decode(&settings)
	client := &http.Client{}
	data := url.Values{}
	data.Set("token", accessToken)
	req, err := http.NewRequest(
		http.MethodPost,
		settings.IntrospectionURL,
		strings.NewReader(data.Encode()))
	tokenCache := authEntities.TokenCache{}
	if err != nil {
		return tokenCache, err
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.SetBasicAuth(settings.ClientID, settings.ClientSecret)
	resp, err := client.Do(req)
	if err != nil {
		return tokenCache, err
	}
	tokenResponse := make(map[string]interface{})
	json.NewDecoder(resp.Body).Decode(&tokenResponse)
	if tokenResponse["active"].(bool) == true {
		tokenCache.AccessToken = accessToken
		tokenCache.ClientID = tokenResponse["client_id"].(string)
		tokenCache.ExpiryTime = int64(tokenResponse["exp"].(float64))
		if sub, ok := tokenResponse["sub"]; ok {
			tokenCache.Sub = sub.(string)
		}
		if roles, ok := tokenResponse["roles"]; ok {
			tokenCache.Roles = roles.([]interface{})
		}
		tokenCache.Scope = tokenResponse["scope"].([]interface{})
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()
		tokenCollection.InsertOne(ctx, tokenCache)
		return tokenCache, nil
	}
	if tokenResponse["active"].(bool) == false {
		return tokenCache, errors.New("TOKEN_INACTIVE")
	}
	return tokenCache, nil
}

// RoleGuard allows users with passed list of roles
func RoleGuard(roles []string) common.Middleware {
	return func(f http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			token := gorillaContext.Get(r, "token").(authEntities.TokenCache)
			intersectRoles := intersect.Sorted(token.Roles, roles).([]interface{})
			if len(intersectRoles) > 0 {
				f(w, r)
			} else {
				var message interface{}
				json.Unmarshal([]byte(`{"Error":"Forbidden"}`), &message)
				common.RespondWithError(w, http.StatusForbidden, message)
			}
		}
	}
}
