package entities

import (
	"net/http"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// AuthSettings stores server settings
type AuthSettings struct {
	ID               primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	AuthServerURL    string             `validate:"required,url" json:"authServerURL" bson:"authServerURL"`
	AppURL           string             `validate:"required,url" json:"appURL" bson:"appURL"`
	ClientID         string             `validate:"required,uuid" json:"clientId" bson:"clientId"`
	ClientSecret     string             `validate:"required" json:"clientSecret" bson:"clientSecret"`
	ProfileURL       string             `json:"profileURL,omitempty" bson:"profileURL,omitempty"`
	TokenURL         string             `json:"tokenURL,omitempty" bson:"tokenURL,omitempty"`
	IntrospectionURL string             `json:"introspectionURL,omitempty" bson:"introspectionURL,omitempty"`
	AuthorizationURL string             `json:"authorizationURL,omitempty" bson:"authorizationURL,omitempty"`
	CallbackURLs     []string           `json:"callbackURLs,omitempty" bson:"callbackURLs,omitempty"`
	RevocationURL    string             `json:"revocationURL,omitempty" bson:"revocationURL,omitempty"`
}

// AuthSettingsCollection string token for mongodb collection
var AuthSettingsCollection = "server_settings"

// ParseSetupPayload parse setup request payload
func ParseSetupPayload(r *http.Request) AuthSettings {
	r.ParseForm()
	settings := AuthSettings{
		AuthServerURL: r.FormValue("authServerURL"),
		ClientID:      r.FormValue("clientId"),
		ClientSecret:  r.FormValue("clientSecret"),
		AppURL:        r.FormValue("appURL"),
	}
	return settings
}
