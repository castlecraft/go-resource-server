package entities

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// TokenCache Cache BearerToken Data for reuse
type TokenCache struct {
	ID           primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	AccessToken  string             `json:"accessToken,omitempty" bson:"accessToken,omitempty"`
	RefreshToken string             `json:"refreshToken" bson:"refreshToken"`
	Sub          string             `json:"sub" bson:"sub"`
	ClientID     string             `json:"clientId" bson:"clientId"`
	ExpiryTime   int64              `json:"expiryTime" bson:"expiryTime"`
	Roles        []interface{}      `json:"roles" bson:"roles"`
	Scope        []interface{}      `json:"scope" bson:"scope"`
}

// TokenCacheCollection string token for mongodb collection
var TokenCacheCollection = "token_cache"
