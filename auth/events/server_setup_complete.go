package events

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	authEntities "go-resource-server/auth/entities"

	"go.mongodb.org/mongo-driver/mongo"
)

// ServerSetupComplete Event fires after command SetupServer is validated
func ServerSetupComplete(w http.ResponseWriter, r *http.Request, collection *mongo.Collection, settings authEntities.AuthSettings) {
	// Append CallbackURLs
	settings.CallbackURLs = []string{
		settings.AppURL + "/index.html",
		settings.AppURL + "/silent-refresh.html",
	}
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	result, _ := collection.InsertOne(ctx, settings)
	json.NewEncoder(w).Encode(result)
}
