package policies

import (
	"encoding/json"
	"errors"
	authEntities "go-resource-server/auth/entities"
	"net/http"
)

// ValidateOpenIDConfiguration set OpenID configuration from AuthServerURL
func ValidateOpenIDConfiguration(settings authEntities.AuthSettings) (authEntities.AuthSettings, error) {
	resp, err := http.Get(settings.AuthServerURL + "/.well-known/openid-configuration")
	if err != nil {
		return settings, errors.New(`{"Error":"Unable to retrieve openid-configuration"}`)
	}
	defer resp.Body.Close()
	openIDConfiguration := make(map[string]string)
	json.NewDecoder(resp.Body).Decode(&openIDConfiguration)
	settings.ProfileURL = openIDConfiguration["userinfo_endpoint"]
	settings.TokenURL = openIDConfiguration["token_endpoint"]
	settings.IntrospectionURL = openIDConfiguration["introspection_endpoint"]
	settings.AuthorizationURL = openIDConfiguration["authorization_endpoint"]
	settings.RevocationURL = openIDConfiguration["revocation_endpoint"]
	return settings, nil
}
