package policies

import (
	"encoding/json"
	"errors"
	"net/http"

	authEntities "go-resource-server/auth/entities"

	"github.com/thedevsaddam/govalidator"
)

// ValidateSetupPayload Policy to validate setup payload data
func ValidateSetupPayload(w http.ResponseWriter, r *http.Request) (authEntities.AuthSettings, error) {
	settings := authEntities.ParseSetupPayload(r)
	rules := govalidator.MapData{
		"authServerURL": []string{"required", "url"},
		"clientId":      []string{"required", "uuid"},
		"clientSecret":  []string{"required"},
		"appURL":        []string{"required", "url"},
	}
	opts := govalidator.Options{
		Data:  &settings,
		Rules: rules,
	}
	v := govalidator.New(opts)
	e := v.ValidateStruct()
	if len(e) > 0 {
		data, _ := json.Marshal(e)
		return settings, errors.New(string(data))
	}
	return settings, nil
}
