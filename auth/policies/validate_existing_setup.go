package policies

import (
	"context"
	"errors"
	"net/http"
	"time"

	authEntities "go-resource-server/auth/entities"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

// ValidateExistingSetup Policy to validate existing setup
func ValidateExistingSetup(collection *mongo.Collection, w http.ResponseWriter) (int, error) {
	var existing []authEntities.AuthSettings
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	cursor, err := collection.Find(ctx, bson.M{})
	if err != nil {
		return http.StatusInternalServerError, err
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var settings authEntities.AuthSettings
		cursor.Decode(&settings)
		existing = append(existing, settings)
	}
	if err := cursor.Err(); err != nil {
		return http.StatusInternalServerError, err
	}
	if len(existing) > 0 {
		setupComplete := `{"Error":"Setup Already Complete"}`
		return http.StatusBadRequest, errors.New(setupComplete)
	}
	return http.StatusOK, nil
}
