package commands

import (
	"net/http"

	authAggregates "go-resource-server/auth/aggregates"

	"go.mongodb.org/mongo-driver/mongo"
)

// SetupServerCommand command to setup server
func SetupServerCommand(w http.ResponseWriter, r *http.Request, database *mongo.Database) (int, error) {
	statusCode, err := authAggregates.SetupServer(w, r, database)
	if err != nil {
		return statusCode, err
	}
	return http.StatusOK, nil
}
