package aggregates

import (
	"net/http"

	authEntities "go-resource-server/auth/entities"
	authEvents "go-resource-server/auth/events"
	authPolicies "go-resource-server/auth/policies"

	"go.mongodb.org/mongo-driver/mongo"
)

// SetupServer initialize collection and setup server
func SetupServer(w http.ResponseWriter, r *http.Request, db *mongo.Database) (int, error) {
	collection := db.Collection(authEntities.AuthSettingsCollection)
	// Validate Policies
	settings, err := authPolicies.ValidateSetupPayload(w, r)
	if err != nil {
		return http.StatusBadRequest, err
	}
	statusCode, err := authPolicies.ValidateExistingSetup(collection, w)
	if err != nil {
		return statusCode, err
	}
	settings, err = authPolicies.ValidateOpenIDConfiguration(settings)
	if err != nil {
		return http.StatusServiceUnavailable, err
	}
	// Fire Event
	authEvents.ServerSetupComplete(w, r, collection, settings)
	return statusCode, nil
}
