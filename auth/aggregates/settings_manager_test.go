package aggregates_test

import (
	"fmt"
	"testing"
)

type ValidateExistingSetup struct {
	Name string
}

func (m *ValidateExistingSetup) MockName() string {
	return "Mock" + m.Name
}

var x interface{}

func TestSetupServer(t *testing.T) {
	x = ValidateExistingSetup{Name: "Mock"} // x is a interface{}
	g, ok := x.(ValidateExistingSetup)      // we have to check that x is a Greeter...
	if ok {
		fmt.Println(g.MockName()) // ...before being able to call Hello()
	}
}
