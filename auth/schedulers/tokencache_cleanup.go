package schedulers

import (
	"context"
	"log"
	"time"

	entities "go-resource-server/auth/entities"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

// DeleteExpiredTokensScheduler simple Job to delete expired tokens from cache
func DeleteExpiredTokensScheduler(database *mongo.Database) {
	collection := database.Collection(entities.TokenCacheCollection)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	_, err := collection.DeleteMany(ctx, bson.M{
		"expiryTime": bson.M{"$lte": time.Now().Unix()},
	})
	if err != nil {
		log.Println(err.Error())
	}
}
