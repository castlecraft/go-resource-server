package common

import (
	"encoding/json"
	"net/http"
)

// ServerMessage to display response message from server
type ServerMessage struct {
	Message string
}

// Middleware used to chain http middlewares
type Middleware func(http.HandlerFunc) http.HandlerFunc

// RespondWithError Respond HTTP Request with Error
func RespondWithError(w http.ResponseWriter, code int, payload interface{}) {
	RespondWithJSON(w, code, payload)
}

// RespondWithJSON Respond HTTP Request with payload JSON
func RespondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}

// Chain Chains list of middlewares
func Chain(f http.HandlerFunc, middlewares ...Middleware) http.HandlerFunc {
	// Refer https://gowebexamples.com/advanced-middleware/
	for _, m := range middlewares {
		f = m(f)
	}
	return f
}
