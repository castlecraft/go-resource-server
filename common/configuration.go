package common

import (
	"log"

	"github.com/caarlos0/env"
	"github.com/joho/godotenv"
)

// EnvConfig stores environment variables
type EnvConfig struct {
	DatabaseName string `env:"DB_NAME"`
	DatabaseHost string `env:"DB_HOST"`
}

// ConfigureEnvironment parse .env file and return EnvConfig
func ConfigureEnvironment() EnvConfig {
	if err := godotenv.Load(); err != nil {
		log.Println("File .env not found, reading configuration from ENV")
	}
	var cfg EnvConfig
	if err := env.Parse(&cfg); err != nil {
		log.Fatalln("Failed to parse ENV")
	}
	return cfg
}
