package main_test

import (
	main "go-resource-server"
	common "go-resource-server/common"
	"testing"
)

func TestMain(t *testing.T) {
	a := main.App{}
	cfg := common.ConfigureEnvironment()
	a.Initialize(cfg.DatabaseHost, cfg.DatabaseName)
	if cfg.DatabaseHost == "" {
		t.Errorf("DB host not found, got: %v, expected: DB_HOST.", cfg.DatabaseHost)
	}
	if cfg.DatabaseName == "" {
		t.Errorf("DB name not found, got: %v, expected: DB_HOST.", cfg.DatabaseHost)
	}
}
